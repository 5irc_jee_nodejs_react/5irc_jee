package fr.cpe.ejb;

import common.UserModel;

import javax.ejb.Local;

@Local
public interface MessageReceiver {

    /**
     * Permet de récupérer un objet user depuis la Queue
     * @return UserModel
     * */
    public UserModel receiveMessage();

}
