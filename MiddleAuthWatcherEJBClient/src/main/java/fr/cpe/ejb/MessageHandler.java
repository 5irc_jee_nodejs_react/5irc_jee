package fr.cpe.ejb;

import common.UserModel;

import javax.ejb.Local;
import javax.json.JsonObject;

@Local
public interface MessageHandler {

    /**
     * Permet d'envoyer un message a l'EJB Message Driven via un topic
     * @param message
     * */
    void sendMessage(String message);

    /**
     * Permet d'envoyer un user a l'EJB Message Driven via un topic
     * @param user
     * */
    void sendMessage(UserModel user);
}
