package common;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Classe effectuant les appels en base de données
 * */
@ApplicationScoped
public class UserRepository {
    @Inject
    private EntityManager em;

    /**
     * Récupération d'un utilisateur grace a à Id
     * @param id
     * @return UserModel
     * */
    public UserModel findById(Long id) {
        return em.find(UserModel.class, id);
    }


    /**
     * Récupération d'un utilisateur grace à ses crédentials
     * @param login
     * @param password
     * @return UserModel
     * */
    public UserModel findByCredentials(String login, String password) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<UserModel> criteria = cb.createQuery(UserModel.class);
        Root<UserModel> user = criteria.from(UserModel.class);
        criteria.select(user).where(cb.equal(user.get("login"), login),
                cb.and(cb.equal(user.get("password"), password)));
        try {
            return em.createQuery(criteria).getSingleResult();
        } catch(NoResultException nre){
            return null;
        }
    }
}
