package common;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.logging.Logger;

@Stateless
public class UserManager {
    @Inject
    private Logger log;

    @Inject
    private EntityManager em;

    @Inject
    private UserRepository repo;

    public UserModel login(UserModel user) {
        log.info("login " + user.getLogin());
        UserModel userQuery = repo.findByCredentials(user.getLogin(),user.getPassword());
        return userQuery;
    }
}
