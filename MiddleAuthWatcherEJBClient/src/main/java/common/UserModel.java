package common;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Bean User
 * Regroupe toutes les informations sur un utilisateur
 * */
@Entity
@Table(name="User")
public class UserModel implements Serializable {

    /** Default value included to remove warning. Remove or modify at will. **/
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "surName")
    private String surName;

    @Column(name = "role")
    private String role;

    public UserModel(String login, String pwd){
        this.setLogin(login);
        this.setPassword(pwd);
    }

    public UserModel(){
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString(){
        return "[UserModel] : login : " + this.login
                + ", password : " + this.password
                + ", lastName : " + this.lastName
                + ", surName : " + this.surName
                + ", role : " + this.role;
    }
}
