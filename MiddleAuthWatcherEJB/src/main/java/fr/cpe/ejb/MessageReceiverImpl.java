package fr.cpe.ejb;

import common.UserModel;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;

@Stateless
public class MessageReceiverImpl implements MessageReceiver{

    @Inject
    JMSContext context;

    /**
     * Queue utilisée pour recevoir les informations
     * depuis l'eJB Message Driven
     * */
    @Resource(name = "java:/ProjetQueue")
    Queue queue;

    /**
     * Permet de récupérer un objet user depuis la Queue
     * @return UserModel usrModel
     * */
    @Override
    public UserModel receiveMessage() {
        UserModel usrModel = null;
        try {
            Message receiver = context.createConsumer(queue).receive();
            usrModel = (UserModel) ((ObjectMessage) receiver).getObject();
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return usrModel;
    }
}
