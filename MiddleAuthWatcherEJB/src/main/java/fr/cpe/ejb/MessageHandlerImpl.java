package fr.cpe.ejb;

import common.UserModel;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Topic;
import javax.json.JsonObject;

@Stateless
public class MessageHandlerImpl implements MessageHandler {

    /**
     * Contexte JMS
     * */
    @Inject
    JMSContext context;

    /**
     * Topic utilisé pour transmettre les information à l'eJB Message Driven
     * */
    @Resource(name = "java:/ProjetTopic")
    Topic topic;

    /**
     * Permet d'envoyer un message a l'EJB Message Driven via un topic
     * @param message
     * */
    @Override
    public void sendMessage(String message) {
        context.createProducer().send(topic, message);
    }

    /**
     * Permet d'envoyer un user a l'EJB Message Driven via un topic
     * @param user
     * */
    @Override
    public void sendMessage(UserModel user) {
        ObjectMessage msg = context.createObjectMessage();
        try {
            msg.setObject(user);
            context.createProducer().send(topic,msg);

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
