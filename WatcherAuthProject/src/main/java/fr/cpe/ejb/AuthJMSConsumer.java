package fr.cpe.ejb;

import common.UserManager;
import common.UserModel;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.*;
import java.util.logging.Logger;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "ProjetTopic"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")  })

public class AuthJMSConsumer implements MessageListener
{
    @Inject
    JMSContext context;

    @Resource(name = "java:/ProjetQueue")
    Queue queue;

    @Inject
    UserManager usrManager;

    private static Logger logger = Logger.getLogger(AuthJMSConsumer.class.getName());

    @Override
    public void onMessage(Message message) {

        try {

            if (message instanceof TextMessage) {
                String msg = ((TextMessage) message).getText();
                //logger.info(msg);
                context.createProducer().send(queue,msg);

            }else if(message instanceof ObjectMessage) {

                UserModel user = usrManager.login((UserModel) ((ObjectMessage) message).getObject());
                context.createProducer().send(queue,user);

            }else {
                logger.warning("Message of wrong type: " +
                        message.getClass().getName());
            }
        } catch (JMSException e) {
            e.printStackTrace();
        } catch (Throwable te) {
            te.printStackTrace();
        }
    }
}
