package fr.cpe.rest;

import javax.json.JsonObject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Service de traitement des informations d'authentification
 *
 * */
@Path("/WatcherAuth")
public interface WatcherAuthService  {

    /**
     * Methode post
     * Recuperation d'un User au format Json
     * */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    String post(JsonObject userInf);

}
