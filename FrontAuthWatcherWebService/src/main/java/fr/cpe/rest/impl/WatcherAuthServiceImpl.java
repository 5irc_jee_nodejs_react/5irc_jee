package fr.cpe.rest.impl;

import common.UserModel;
import fr.cpe.ejb.MessageHandler;
import fr.cpe.ejb.MessageReceiver;
import fr.cpe.rest.WatcherAuthService;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.logging.Logger;

/**
 * Implementation du service de traitement des informations
 * d'authentification
 * */
public class WatcherAuthServiceImpl implements WatcherAuthService {

    /**
     * EJB utilisé pour envoyer les informations d'authent à l'EJB
     * Message Driven
     * */
    @EJB
    MessageHandler messageHandler;

    /**
     * EJB utilisé pour recevoir les informations d'utilisateur depuis l'EJB
     * Message Driven
     * */
    @EJB
    MessageReceiver messageReceiver;

    /**
     * Logger
     * */
    private static Logger logger = Logger.getLogger(WatcherAuthServiceImpl.class.getName());

    /**
     * Methode post
     * Recuperation d'un User au format Json
     * Permet d'authentifier un user
     * */
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public String post(JsonObject userInf) {
        JsonObject response;
        try {


            // Recuperation login/pwd dans le body de la requete
            String login = userInf.getString("login");
            String password = userInf.getString("pwd");

            // Initialisation d'un objet UserModel
            UserModel user = new UserModel(login, password);

            // Envoi des informations utilisateur au back
            messageHandler.sendMessage(user);

            // Reception des informations d'authent depuis le back
            user = messageReceiver.receiveMessage();

            // Creation de l'objet JSON de réponse si succès de l'authent
            response = Json.createObjectBuilder()
                    .add("login", user.getLogin())
                    .add("validAuth", "true")
                    .add("role",user.getRole())
                    .build();

        }catch(Exception e){
            logger.info(e.toString());

            // Creation de l'objet JSON de réponse si echec de l'authent
            response = Json.createObjectBuilder()
                    .add("login", userInf.getString("login"))
                    .add("validAuth", "false")
                    .add("role","N/A")
                    .build();
        }

        return response.toString();
    }
}
