# 5irc_jee

Sous projet contenant la partie JEE :
- EJB
- Webservices
- Topic
- Queue
- Message Driven

1. Configuration :

 - Remplacer le fichier présent dans opt/cpe/wildfly/standalone/configuration/standalone-full.xml 
 par celui présent dans le repos git. 
 - dans intellij, aller dans la configuration du serveur et modifer le profil de lancement en standalone-full.
 
2. Schema :

Dans mysql_workbench, créer un nouveau schéma de base de données appelé "projet".

3. Serveur :

Démarrer le serveur JBOSS.

4. Table User:

Insérer un utilisateur grâce à cette ligne :
INSERT INTO `projet`.`User` (`user_id`, `lastName`, `login`, `password`, `role`, `surName`) VALUES ('1', 'Admin', 'admin', 'admin', 'admin', 'admin');'

5. Test Authentification :

demander l'authentification d'un user avec une requête post et un json contenant 
le login et le password à cette URL :
localhost:8080/FrontAuthWatcherWebService/rest/WatcherAuth

curl --request POST \
  --url http://127.0.0.1:8080/FrontAuthWatcherWebService/rest/WatcherAuth \
  --header 'Content-Type: application/json' \
  --data '{
	"login":"admin",
	"pwd":"admin"
}'

le validAuth dans le json retour doit être à "true".